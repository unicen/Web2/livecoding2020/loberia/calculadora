<?php


function getAbout($developer = null) {

    $urlSite = 'http://' . $_SERVER["SERVER_NAME"] . dirname($_SERVER["PHP_SELF"]). "/";
    
    $html = '<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <base href="' . $urlSite .'" target="_blank">
            <link rel="stylesheet" href="css/style.css">
            <title>Calculadora | About</title>
        </head>
        <body>
        
            <nav id="main-nav">
                <ul>
                    <li><a href="index.html">Calculadora</a></li>
                    <li><a href="pi">Número Pi</a></li>
                    <li><a href="about">Sobre Nosotros</a></li>
                </ul>
            </nav>
        
            <h1>Acerca de nosotros...</h1>
        
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex voluptatem, saepe accusantium eum iusto distinctio ea accusamus, unde totam blanditiis eveniet temporibus quos debitis ullam non vel dolor, dolores tenetur!</p>
            
            <ul>
                <li><a href="about/francisco">Francisco</a></li>
                <li><a href="about/julio">Julio</a></li>
                <li><a href="about/soledad">Soledad</a></li>
            </ul>';

    if ($developer != null) {

        if ($developer == 'francisco') {
            $html .= '<h2>Francisco</h2>';
            $html .= '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>';
            $html .= '<img src="img/img_avatar_h.png" />';
        }
        if ($developer == 'julio') {
            $html .= '<h2>Julio</h2>';
            $html .= '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>';
            $html .= '<img src="img/img_avatar_h.png" />';
        }
        if ($developer == 'soledad') {
            $html .= '<h2>Soledad</h2>';
            $html .= '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>';
            $html .= '<img src="img/img_avatar_m.png" />';
        }
    }

    $html .= '</body>
    </html>';

    return $html;
}


?>