<?php

function suma($a, $b) {
    $resultado = $a + $b;
    return $resultado;
}

function getSuma($operador1, $operador2) {
    $html = '<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>About</title>
    </head>
    <body>';

    if(isset($operador1) && isset($operador2)) {
        $resultado = suma($operador1, $operador2);
        $html .= "<h2> Cuenta: " . $operador1 . " + " . $operador2 . "</h2>";
        $html .= "<h2> Resultado: " . $resultado . "</h2>";
    }

    $html.='</body>
    </html>';

    return $html;

}