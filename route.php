<?php
require_once 'operaciones.php';
require_once 'sumar.php';
require_once 'pi.php';
require_once 'about.php';

// operacion/parametro1/parametro2
// suma/2/2
// about/julio

$action = $_GET["action"];

if($action == "") {
    echo "vacio";
}
else {
    if(isset($action)){
        $partesURL = explode("/", $action);
        //["operacion", "parametro1", "parametro2"]

        $operacion = $partesURL[0];

        switch($operacion) {
            case 'suma':
                echo getSuma($partesURL[1], $partesURL[2]);
                break;
            case 'resta':
                echo resta($partesURL[1], $partesURL[2]);
                break;
            case 'multiplicacion':
                echo multiplicacion($partesURL[1], $partesURL[2]);
                break;
            case 'division':
                echo division($partesURL[1], $partesURL[2]);
                break;
            case 'pi':
                echo getPi();
                break;
            case 'about':
                if(isset($partesURL[1])){
                    echo getAbout($partesURL[1]);
                }
                else {
                    echo getAbout(null);
                }
                break;
            default:
                echo "Error! Operación no permitida.";
                break;
        }
    }
}


?>