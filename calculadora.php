<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style.css">
    <title>Calculadora</title>
</head>
<body>
    <nav id="main-nav">
        <ul>
            <li><a href="index.html">Calculadora</a></li>
            <li><a href="pi.php">El número Pi</a></li>
            <li><a href="about.php">About</a></li>
        </ul>
    </nav>

    <?php
    require_once 'operaciones.php';

    if (!is_numeric($_REQUEST['operando1']) || !is_numeric($_REQUEST['operando2']) || empty($_REQUEST['operacion'])) {
        echo "ERROR: faltan datos.";
        die();
    }


    $op1 = $_REQUEST['operando1'];
    $op2 = $_REQUEST['operando2'];
    $operacion = $_REQUEST['operacion'];

    switch($operacion) {
        case 'suma':
            echo suma($op1, $op2);
            break;
        case 'resta':
            echo resta($op1, $op2);
            break;
        case 'multiplicacion':
            echo multiplicacion($op1, $op2);
            break;
        case 'division':
            echo division($op1, $op2);
            break;
        default:
            echo "Error! Operación no permitida.";
            break;
    }
    ?>

</body>
</html>
